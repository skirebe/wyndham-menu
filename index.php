<?php    
header('Location: https://wyndhamcr.com/Menu/');
return;

?>


<!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Site Metas -->
    <!--se cambio por v-->
    <title>Menu WyndHam</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Site CSS -->
    <link rel="stylesheet" href="css/stylee.css">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">

    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">

    <!-- Font awesome -->
    <link href="assets/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!-- Slick slider -->
    <link rel="stylesheet" type="text/css" href="assets/css/slick.css">
    <!-- Date Picker -->
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-datepicker.css">
    <!-- Gallery Lightbox -->
    <link href="assets/css/magnific-popup.css" rel="stylesheet">
    <!-- Theme color -->
    <link id="switcher" href="assets/css/theme-color/default-theme.css" rel="stylesheet">

    <!-- Main style sheet -->
    <link href="stylee.css" rel="stylesheet">


    <!-- Google Fonts -->

    <!-- Prata for body  -->
    <link href='https://fonts.googleapis.com/css?family=Prata' rel='stylesheet' type='text/css'>
    <!-- Tangerine for small title -->
    <link href='https://fonts.googleapis.com/css?family=Tangerine' rel='stylesheet' type='text/css'>
    <!-- Open Sans for title -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>



</head>

<body>
    <!-- Start header -->
    <header class="top-navbar">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container">
                <a class="navbar-brand" href="index.html">
                    <img width="300" src="images/LOGO-WYNDHAM.png" alt="" />
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbars-rs-food" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
				  <span class="navbar-toggler-icon"></span>
				</button>
                <div class="collapse navbar-collapse" id="navbars-rs-food">
                    <ul class="navbar-nav ml-auto">
                        <section id="banner" class="navbar-nav ml-auto">
                            <li class="nav-item"><a class="nav-link more" href="#one">Platillos<br><p style=" font-size: 12px;font-weight: 600; text-align:center;">Main Course</p></a></li>
                            <li class="nav-item"><a class="nav-link more" href="#three">Postres<br><p style=" font-size: 12px;font-weight: 600; text-align:center;">Desserts</p></a></li>
                            <!-- <li class="nav-item"><a class="nav-link more" href="#two">Desayuno<br><p style=" font-size: 12px;font-weight: 600; text-align:center;">Breakfast</p></a></li> -->
                            <li class="nav-item"><a class="nav-link more" href="#Ejecutivo">Ejecutivo<br><p style=" font-size: 12px;font-weight: 600; text-align:center;">Executive</p></a></li>
                            <li class="nav-item"><a class="nav-link more" href="#four">Bebidas<br><p style=" font-size: 12px;font-weight: 600; text-align:center;">Beverage</p></a></li>
                        </section>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <section id="cero">
        <section id="mu-reservation">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="mu-reservation-area">
                            <img src="./imgnuevas/pngmenu.png">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="mu-counter">
            <div class="mu-counter-overlay">
                <div class="container">
                    <div class="col-md-12">

                        <center>
                            <section >
                                <!-- <img width="100%" height="90%" src="./banner/WyndhamSep.jpg"> -->
                                <img width="100%" height="90%" src="./banner/enero.jpg">
                                <br>
                                <br>
                                <br>
                        </center>

                        <center>
                            <section id="one">
                                <img width="100%" height="90%" src="./8102121/PLATILLOS.jpg">
                        </center>

                        </li>
                        <br>


                        <center>
                            <section id="three">
                                <img width="100%" height="90%" src="./imgnuevas/POSTRES.jpg">
                        </center>
                        </li>
                        <br>

                        <!-- <center>
                            <section id="two">
                                <img width="100%" height="90%" src="./imgnuevas/DESAYUNO.jpg">
                        </center> -->
                        </li>
                        </li>
                        <br>
                        <center>
                            <section id="Ejecutivo">
                                         <img width="100%" height="90%" src="./ejecutivo/ejecutivo.png">

                                <!-- <img width="100%" height="90%" src="./imgnuevas//ejecutivonuevo.jpg"> -->
                           

                        </center>
                        </li>
                        <br>

                        <center>
                            <section id="four">
                                <img width="100%" height="90%" src="images/bebidasoficial.jpg">
                        </center>
                        </li>
                        </ul>
                    </div>
                </div>
            </div>
            </div>
            </div>
            </section>

            <a href="#" id="back-to-top" title="Back to top" style="display: none;"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></a>

            <!-- ALL JS FILES -->
            <script src="js/jquery-3.2.1.min.js"></script>
            <script src="js/popper.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
            <!-- ALL PLUGINS -->
            <script src="js/jquery.superslides.min.js"></script>
            <script src="js/images-loded.min.js"></script>
            <script src="js/isotope.min.js"></script>
            <script src="js/baguetteBox.min.js"></script>
            <script src="js/form-validator.min.js"></script>
            <script src="js/contact-form-script.js"></script>
            <script src="js/custom.js"></script>
            <script src="assets/js/jquery.min.js"></script>
            <script src="assets/js/jquery.scrolly.min.js"></script>
            <script src="assets/js/jquery.scrollex.min.js"></script>
            <script src="assets/js/skel.min.js"></script>
            <script src="assets/js/util.js"></script>
            <script src="assets/js/main.js"></script>

            <head>
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <style>
                    #myImg {
                        border-radius: 5px;
                        cursor: pointer;
                        transition: 0.3s;
                    }
                    
                    #myImg:hover {
                        opacity: 0.7;
                    }
                    /* The Modal (background) */
                    
                    .modal {
                        display: none;
                        /* Hidden by default */
                        position: fixed;
                        /* Stay in place */
                        z-index: 1;
                        /* Sit on top */
                        padding-top: 250px;
                        /* Location of the box */
                        left: 0;
                        top: 0;
                        width: 100%;
                        /* Full width */
                        height: 100%;
                        /* Full height */
                        overflow: auto;
                        /* Enable scroll if needed */
                        background-color: rgb(0, 0, 0);
                        /* Fallback color */
                        background-color: rgba(0, 0, 0, 0.9);
                        /* Black w/ opacity */
                    }
                    /* Modal Content (image) */
                    
                    .modal-content {
                        margin: auto;
                        display: block;
                        width: 80%;
                        max-width: 700px;
                    }
                    /* Caption of Modal Image */
                    
                    #caption {
                        margin: auto;
                        display: block;
                        width: 80%;
                        max-width: 700px;
                        text-align: center;
                        color: #ccc;
                        padding: 10px 0;
                        height: 150px;
                    }
                    /* Add Animation */
                    
                    .modal-content,
                    #caption {
                        -webkit-animation-name: zoom;
                        -webkit-animation-duration: 0.6s;
                        animation-name: zoom;
                        animation-duration: 0.6s;
                    }
                    
                    @-webkit-keyframes zoom {
                        from {
                            -webkit-transform: scale(0)
                        }
                        to {
                            -webkit-transform: scale(1)
                        }
                    }
                    
                    @keyframes zoom {
                        from {
                            transform: scale(0)
                        }
                        to {
                            transform: scale(1)
                        }
                    }
                    /* The Close Button */
                    
                    .close {
                        position: absolute;
                        top: 180px;
                        right: 30px;
                        color: #f1f1f1;
                        font-size: 40px;
                        font-weight: bold;
                        transition: 0.3s;
                    }
                    
                    .close:hover,
                    .close:focus {
                        color: #bbb;
                        text-decoration: none;
                        cursor: pointer;
                    }
                    /* 100% Image Width on Smaller Screens */
                    
                    @media only screen and (max-width: 700px) {
                        .modal-content {
                            width: 100%;
                        }
                    }
                </style>
                <!-- The Modal -->
                 <!-- <div id="myModal" class="modal">
                    <span id="myImg2" class="close">&times;</span>
                    <img class="modal-content" id="img01">
                    <div id="caption"></div>
                </div>
                <img style="display: none" id="myImg" src="./images/popup/2.jpg" alt="Menú" style="width:100%;max-width:300px">
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> -->
                <!-- <script>
                    
                    var modal = document.getElementById("");

                    // Get the image and insert it inside the modal - use its "alt" text as a caption
                    var img = document.getElementById("myImg");
                    var modalImg = document.getElementById("img01");
                    var captionText = document.getElementById("caption");
                    img.onclick = function() {
                        modal.style.display = "block";
                        modalImg.src = this.src;
                        captionText.innerHTML = this.alt;
                    }

                    // Get the <span> element that closes the modal
                    var span = document.getElementsByClassName("close")[0];

                    // When the user clicks on <span> (x), close the modal
                    span.onclick = function() {
                            modal.style.display = "none";
                        }
                        // abre el popups
                    $(document).ready(function() {
                        // indicamos que se ejecuta la funcion a los 5 segundos de haberse
                        // cargado la pagina
                        setTimeout(clickbutton, 5000);

                        function clickbutton() {
                            // simulamos el click del mouse en el boton del formulario
                            $("#myImg").click();

                        }
                        $('#myImg').on('click', function() {
                            console.log('action button clicked');
                        });
                    });

                    // // cierra el popups
                    // $(document).ready(function() {
                    //     // indicamos que se ejecuta la funcion a los 5 segundos de haberse
                    //     // cargado la pagina
                    //     setTimeout(clickbutton, 30000);

                    //     function clickbutton() {
                    //         // simulamos el click del mouse en el boton del formulario
                    //         $("#myImg2").click();

                    //     }
                    //     $('#myImg2').on('click', function() {
                    //         console.log('action button clicked');
                    //     });
                    // });
                </script> -->
</body>

</html>